package ru.konovalov.tm.api;

import ru.konovalov.tm.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {
}
