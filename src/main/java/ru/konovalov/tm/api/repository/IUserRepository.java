package ru.konovalov.tm.api.repository;

import ru.konovalov.tm.api.IRepository;
import ru.konovalov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeUser(User user);

    User removeUserById(String id);

    User removeUserByLogin(String login);

    boolean existsByLogin(String login);

    boolean existsByEmail(String email);
}
