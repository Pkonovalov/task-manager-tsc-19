package ru.konovalov.tm.api.service;

import ru.konovalov.tm.command.AbstractCommand;
import ru.konovalov.tm.model.Command;

import java.util.Collection;

public interface ICommandService {

    Collection<AbstractCommand> getCommands();

    Collection<AbstractCommand> getArguments();

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    Collection<String> getListArgumentName();

    Collection<String> getListCommandName();

    void add(AbstractCommand command);

}
