package ru.konovalov.tm.api.entity;

import ru.konovalov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
