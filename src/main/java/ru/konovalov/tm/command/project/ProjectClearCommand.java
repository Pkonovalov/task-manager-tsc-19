package ru.konovalov.tm.command.project;

import ru.konovalov.tm.model.User;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "Delete all projects";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CLEAR]");
        User user = serviceLocator.getAuthService().getUser();
        serviceLocator.getProjectService().clear(user.getId());
    }

}

