package ru.konovalov.tm.command.task;

import ru.konovalov.tm.util.TerminalUtil;

public final class TaskByIndexRemoveCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-remove-by-index";
    }

    @Override
    public String description() {
        return "Remove task by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER INDEX:");
        serviceLocator.getTaskService().removeTaskByIndex(userId, TerminalUtil.nextNumber());
    }

}
