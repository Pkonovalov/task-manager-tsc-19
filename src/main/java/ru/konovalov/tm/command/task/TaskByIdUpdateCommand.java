package ru.konovalov.tm.command.task;

import ru.konovalov.tm.exeption.empty.EmptyNameException;
import ru.konovalov.tm.exeption.entity.TaskNotFoundException;
import ru.konovalov.tm.model.Task;
import ru.konovalov.tm.model.User;
import ru.konovalov.tm.util.TerminalUtil;

import static ru.konovalov.tm.util.TerminalUtil.incorrectValue;
import static ru.konovalov.tm.util.ValidationUtil.isEmpty;

public final class TaskByIdUpdateCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-update-by-id";
    }

    @Override
    public String description() {
        return "Update task by id";
    }

    @Override
    public void execute() {
        final User user = serviceLocator.getAuthService().getUser();
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findOneById(user.getId(), id);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        if (isEmpty(name)) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = serviceLocator.getTaskService().updateTaskById(user.getId(), id, name, description);
        if (taskUpdated == null) incorrectValue();


    }

}
