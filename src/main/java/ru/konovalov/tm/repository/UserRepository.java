package ru.konovalov.tm.repository;

import ru.konovalov.tm.api.repository.IUserRepository;
import ru.konovalov.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findById(final String id) {
        for (User user : list) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User findByLogin(final String login) {
        for (User user : list) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(final String email) {
        for (User user : list) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public User removeUser(final User user) {
        list.remove(user);
        return user;
    }

    @Override
    public User removeUserById(final String id) {
        final User user = findById(id);
        if (user == null) return null;
        list.remove(user);
        return user;
    }

    @Override
    public User removeUserByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        list.remove(user);
        return user;
    }

    @Override
    public boolean existsByLogin(final String login) {
        for (final User user : list) {
            if (login.equals(user.getLogin())) return true;
        }
        return false;
    }

    public boolean existsByEmail(final String email) {
        for (final User user : list) {
            if (email.equals(user.getEmail())) return true;
        }
        return false;
    }
}
