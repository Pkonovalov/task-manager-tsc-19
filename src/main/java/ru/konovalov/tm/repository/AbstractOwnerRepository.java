package ru.konovalov.tm.repository;

import ru.konovalov.tm.api.IOwnerRepository;
import ru.konovalov.tm.model.AbstractOwner;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerRepository <E extends AbstractOwner> extends AbstractRepository<E> implements IOwnerRepository<E> {

    protected final List<E> list = new ArrayList<>();

    public List<E> findAll(final String userId) {
        List<E> entities = new ArrayList<>();
        for (final E entity : list) {
            if (userId.equals(entity.getUserId())) entities.add(entity);
        }
        return entities;
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        final List<E> entities = new ArrayList<>();
        for (E e : list) if (userId.equals(e.getUserId())) entities.add(e);
        entities.sort(comparator);
        return entities;
    }

    @Override
    public E findById(final String userId, final String id) {
        for (E entity : list) {
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public int size(String userId) {
        int i = 0;
        for (final E e : list) {
            if (userId.equals(e.getUserId())) i++;
        }
        return i;
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        for (final E e : list) {
            if (id.equals(e.getId()) && userId.equals(e.getUserId())) return true;
        }
        return false;
    }

    @Override
    public void clear(final String userId) {
        for (int i = list.size(); i-- > 0; ) {
            if (userId.equals(list.get(i).getUserId())) {
                list.remove(i);
            }
        }
    }

    @Override
    public E removeById(final String userId, final String id) {
        final E entity = findById(userId, id);
        if (entity == null) return null;
        list.remove(entity);
        return null;
    }

}