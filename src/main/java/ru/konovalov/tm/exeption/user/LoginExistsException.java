package ru.konovalov.tm.exeption.user;

import ru.konovalov.tm.exeption.AbstractException;

public class LoginExistsException extends AbstractException {

    public LoginExistsException() {
        super("Error! Login already exists...");
    }

}
