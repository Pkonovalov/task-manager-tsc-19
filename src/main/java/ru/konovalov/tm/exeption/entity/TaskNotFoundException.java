package ru.konovalov.tm.exeption.entity;


import ru.konovalov.tm.exeption.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error. Task not found.");
    }

}
