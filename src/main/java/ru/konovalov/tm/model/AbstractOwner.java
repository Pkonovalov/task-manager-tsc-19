package ru.konovalov.tm.model;

import ru.konovalov.tm.api.entity.ITWBS;

public abstract class AbstractOwner extends AbstractEntity implements ITWBS {

    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}

